package me.supercube101.MTracking;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.util.ArrayList;

/**
 * Created by Aaron.
 */
public class Tracker {

    double x;
    double z;
    double y;
    TrackerType type;
    Location centerblock;
    Player[] onlineplayers = Bukkit.getServer().getOnlinePlayers();
    Player initiator;

    public static ArrayList<String> playersNorth = new ArrayList<String>();
    public static ArrayList<String> playersSouth = new ArrayList<String>();
    public static ArrayList<String> playersEast = new ArrayList<String>();
    public static ArrayList<String> playersWest = new ArrayList<String>();

    public Tracker(Location loc, Player p){
        this.x = loc.getBlockX();
        this.z = loc.getBlockZ();
        this.y = loc.clone().subtract(0,1,0).getBlock().getY();
        this.centerblock = loc.clone().subtract(0,1,0).getBlock().getLocation();

        if(loc.clone().subtract(0,1,0).getBlock().getType().equals(Material.DIAMOND_BLOCK)) type = TrackerType.PERMANENT;
        if(loc.clone().subtract(0,1,0).getBlock().getType().equals(Material.OBSIDIAN)) type = TrackerType.TEMPORARY;

        initiator = p;
    }

    public ArrayList<String> getNorthPlayers(){
        return playersNorth;
    }

    public int getDistanceToTrack(Direction direction, World world){

        Material endBlock;
        Material toTravelAlong;

        int distance = 0;

        if(type.equals(TrackerType.PERMANENT)){
            endBlock = Material.GOLD_BLOCK; toTravelAlong = Material.OBSIDIAN;
        }
        else{ endBlock = Material.STONE; toTravelAlong = Material.COBBLESTONE;}


        for(int i = 1; i < 10000; i++ ){

            if(direction.equals(Direction.NORTH)){
                Block b = world.getBlockAt((int) x, (int)y,(int)z - i);

                if(b.getType().equals(endBlock)) { distance++; return distance * 25;}

                if(b.getType().equals(toTravelAlong)) distance++;



                else return 0;
            }

            if(direction.equals(Direction.SOUTH)){
                Block b = world.getBlockAt((int) x, (int)y,(int)z + i);

                if(b.getType().equals(endBlock)){ distance++; return distance * 25;}

                if(b.getType().equals(toTravelAlong)) distance++;

                else return 0;
            }

            if(direction.equals(Direction.EAST)){

                Block b = world.getBlockAt((int) x + i, (int)y,(int)z);

                if(b.getType().equals(endBlock)) { distance++; return distance * 25;}

                if(b.getType().equals(toTravelAlong)) distance++;

                else return 0;
            }

            if(direction.equals(Direction.WEST)){
                Block b = world.getBlockAt((int) x - i, (int)y,(int)z);

                if(b.getType().equals(endBlock)) { distance++; return distance * 25;}

                if(b.getType().equals(toTravelAlong)) distance++;

                else return 0;
            }

        }
        return 0;
    }

       public boolean getIfPlayerIsInDirection(Direction d, Player victim, World world){
        //Handling North -----------------------------------------------------------------------------------------
        if(d.equals(Direction.NORTH)){
        int corner1x = (int) -10000;
        int corner1y = 256;
        int corner1z = (int) z - 1;

            int corner2x = 10000;
            int corner2y = 0;
            int corner2z = (int) z - getDistanceToTrack(Direction.NORTH, world);

            if(corner1x < victim.getLocation().getX() && victim.getLocation().getX() < corner2x){
                if(corner1y > victim.getLocation().getY() && victim.getLocation().getY() > corner2y){
                    if(corner1z > victim.getLocation().getZ() && victim.getLocation().getZ() > corner2z){
                        return true;
                    }
                }
            }
        }
        //SOUTH
           if(d.equals(Direction.SOUTH)){
               int corner1x = (int) -10000;
               int corner1y = 256;
               int corner1z = (int) z + 1;

               int corner2x = (int) 10000;
               int corner2y = 0;
               int corner2z = (int) z + getDistanceToTrack(Direction.SOUTH, world);

               if(corner1x < victim.getLocation().getX() && victim.getLocation().getX() < corner2x){
                   if(corner1y > victim.getLocation().getY() && victim.getLocation().getY() > corner2y){
                       if(corner1z < victim.getLocation().getZ() && victim.getLocation().getZ() < corner2z){
                           return true;
                       }
                   }
               }
           }

        //EAST
           if(d.equals(Direction.EAST)){
               int corner1z = (int) -10000;
               int corner1y = 256;
               int corner1x = (int) x + 1;

               int corner2z = (int) 10000;
               int corner2y = 0;
               int corner2x = (int) x + getDistanceToTrack(Direction.EAST, world);
               if(corner1x < victim.getLocation().getX() && victim.getLocation().getX() < corner2x){
                   if(corner1y > victim.getLocation().getY() && victim.getLocation().getY() > corner2y){
                       if(corner1z < victim.getLocation().getZ() && victim.getLocation().getZ() < corner2z){
                           return true;
                       }
                   }
               }
           }
        //WEST
           if(d.equals(Direction.WEST)){
               int corner1z = (int) 10000;
               int corner1y = 256;
               int corner1x = (int) x - 1;

               int corner2z = (int) -10000;
               int corner2y = 0;
               int corner2x = (int) x - getDistanceToTrack(Direction.WEST, world);
               if(corner1x > victim.getLocation().getX() && victim.getLocation().getX() > corner2x){
                   if(corner1y > victim.getLocation().getY() && victim.getLocation().getY() > corner2y){
                       if(corner1z > victim.getLocation().getZ() && victim.getLocation().getZ() > corner2z){
                           return true;
                       }
                   }
               }
           }
        return false;
    }

    public void trackForPlayer(Player victim){
        for(World world : Bukkit.getWorlds()){
            if(getIfPlayerIsInDirection(Direction.NORTH, victim, world)) playersNorth.add(victim.getName());
            if(getIfPlayerIsInDirection(Direction.SOUTH, victim, world)) playersSouth.add(victim.getName());
            if(getIfPlayerIsInDirection(Direction.EAST, victim, world)) playersEast.add(victim.getName());
            if(getIfPlayerIsInDirection(Direction.WEST, victim, world)) playersWest.add(victim.getName());
        }
    }

    public TrackerType getType(){
        return type;
    }
    

    public void clearTempTracker(Tracker tracker){

            Material endBlock;
            Material toTravelAlong;

            boolean northdun = false;
            boolean southdun = false;
            boolean eastdun = false;
            boolean westdun = false;

            ArrayList<Block> blockstoclear = new ArrayList<Block>();

            endBlock = Material.STONE;
            toTravelAlong = Material.COBBLESTONE;

            ArrayList<Block> northblock = new ArrayList<Block>();
            ArrayList<Block> southblock = new ArrayList<Block>();

            ArrayList<Block> eastblock = new ArrayList<Block>();

            ArrayList<Block> westblock = new ArrayList<Block>();



        for(int i = 1; i < 10000; i++ ){
                
            for(Direction direction: Direction.values()){
                if(direction.equals(Direction.NORTH) && !northdun){
                    Block b = initiator.getWorld().getBlockAt((int) x, (int)y,(int)z - i);

                    if(b.getType().equals(endBlock)){ northblock.add(b); northdun = true;}

                    if(b.getType().equals(toTravelAlong)) northblock.add(b);

                }

                if(direction.equals(Direction.SOUTH)&& !southdun){
                    Block b = initiator.getWorld().getBlockAt((int) x, (int)y,(int)z + i);

                    if(b.getType().equals(endBlock)) { southblock.add(b); southdun = true;}

                    if(b.getType().equals(toTravelAlong)) southblock.add(b);

                }

                if(direction.equals(Direction.EAST)&& !eastdun){

                    Block b = initiator.getWorld().getBlockAt((int) x + i, (int)y,(int)z);

                    if(b.getType().equals(endBlock)) { eastblock.add(b); eastdun = true;}

                    if(b.getType().equals(toTravelAlong)) eastblock.add(b);

                }

                if(direction.equals(Direction.WEST)&& !westdun){
                    Block b = initiator.getWorld().getBlockAt((int) x - i, (int)y,(int)z);

                    if(b.getType().equals(endBlock)) { westblock.add(b);westdun = true;}

                    if(b.getType().equals(toTravelAlong)) westblock.add(b);
                }

            }
                
        }

        northblock.add(centerblock.getBlock());
        southblock.add(centerblock.getBlock());
        eastblock.add(centerblock.getBlock());
        westblock.add(centerblock.getBlock());

        for(Block b : northblock){
            if(!northdun){
                northblock.clear();
                break;
            }
            b.setType(Material.AIR);
        }

        for(Block b : southblock){
            if(!southdun){
                southblock.clear();
                break;
            }
            b.setType(Material.AIR);
        }

        for(Block b : eastblock){
            if(!eastdun){
                eastblock.clear();
                break;
            }
            b.setType(Material.AIR);
        }

        for(Block b : westblock){
            if(!westdun){
                westblock.clear();
                break;
            }
            b.setType(Material.AIR);
        }

        northblock.clear();
        southblock.clear();
        eastblock.clear();
        westblock.clear();
    }
}
