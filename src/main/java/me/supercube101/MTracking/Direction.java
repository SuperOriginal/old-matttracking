package me.supercube101.MTracking;

/**
 * Created by Aaron.
 */
public enum Direction {

    NORTH, SOUTH, EAST, WEST
}
