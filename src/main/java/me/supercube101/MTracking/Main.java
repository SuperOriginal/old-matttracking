package me.supercube101.MTracking;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Aaron.
 */
public class Main extends JavaPlugin{

    String PREFIX = ChatColor.GRAY + "[" + ChatColor.AQUA + "Track" + ChatColor.GRAY + "] ";

    public boolean onCommand(CommandSender sender, Command cmd, String lbl, String[] args){

        if(cmd.getName().equalsIgnoreCase("track")){

            if(!(sender instanceof Player)){
                sender.sendMessage(PREFIX + "Only players can use console commands.");
                return true;
            }

            Player p = (Player) sender;

            if(!p.hasPermission("matt.track")){
                p.sendMessage(ChatColor.RED + "No permission.");
                return true;
            }

            if(args.length == 0){
                p.sendMessage(ChatColor.GRAY + "/track " + ChatColor.AQUA + "all");
                p.sendMessage(ChatColor.GRAY + "/track " + ChatColor.AQUA + "[player]");
                return true;
            }

            if(args[0].equalsIgnoreCase("all")){
                if(!isOnTracker(p)){
                    p.sendMessage(PREFIX + ChatColor.RED + "You are not standing on a valid tracker block.");
                    return true;
                }

                if(!p.getWorld().getName().equals("world")){
                    p.sendMessage(PREFIX + ChatColor.RED + "You must be in the overworld to track!");
                    return true;
                }

                Tracker tracker = new Tracker(p.getLocation(),p);

                boolean valid = false;

                for(Direction direction : Direction.values()){
                    if(tracker.getDistanceToTrack(direction, p.getWorld()) != 0){
                        valid = true;
                    }
                }

                if(!valid){
                    p.sendMessage(PREFIX + ChatColor.RED + "This tracker is not properly set up and won't track anything.");
                    return true;
                }

                for(Player player : Bukkit.getOnlinePlayers()){
                    tracker.trackForPlayer(player);
                }

                StringBuilder northBuilder = new StringBuilder("");
                StringBuilder southBuilder = new StringBuilder("");
                StringBuilder eastBuilder = new StringBuilder("");
                StringBuilder westBuilder = new StringBuilder("");

                for(String str : tracker.getNorthPlayers()){
                    northBuilder.append(ChatColor.AQUA + str).append(" ");
                }
                for(String str : tracker.playersSouth){
                    southBuilder.append(ChatColor.AQUA + str).append(" ");
                }
                for(String str : tracker.playersEast){
                    eastBuilder.append(ChatColor.AQUA + str).append(" ");
                }
                for(String str : tracker.playersWest){
                    westBuilder.append(ChatColor.AQUA + str).append(" ");
                }

                String northfinal = northBuilder.toString().trim();
                String southfinal = southBuilder.toString().trim();
                String eastfinal = eastBuilder.toString().trim();
                String westfinal = westBuilder.toString().trim();



                p.sendMessage(ChatColor.AQUA + "Tracking results for " + args[0].toLowerCase() + ":");
                if(tracker.getDistanceToTrack(Direction.NORTH, p.getWorld()) != 0) p.sendMessage(ChatColor.GRAY + "North" + ChatColor.DARK_GRAY + "<" + ChatColor.GRAY + String.valueOf(tracker.getDistanceToTrack(Direction.NORTH, p.getWorld())) + ChatColor.DARK_GRAY + ">" + ChatColor.GRAY + ": " + northfinal);
                if(tracker.getDistanceToTrack(Direction.SOUTH, p.getWorld()) != 0) p.sendMessage(ChatColor.GRAY + "South" + ChatColor.DARK_GRAY + "<" + ChatColor.GRAY + String.valueOf(tracker.getDistanceToTrack(Direction.SOUTH, p.getWorld())) + ChatColor.DARK_GRAY + ">" + ChatColor.GRAY + ": " + southfinal);
                if(tracker.getDistanceToTrack(Direction.EAST, p.getWorld()) != 0) p.sendMessage(ChatColor.GRAY + "East" + ChatColor.DARK_GRAY + "<" + ChatColor.GRAY + String.valueOf(tracker.getDistanceToTrack(Direction.EAST, p.getWorld())) + ChatColor.DARK_GRAY + ">" + ChatColor.GRAY + ": " + eastfinal);
                if(tracker.getDistanceToTrack(Direction.WEST, p.getWorld()) != 0) p.sendMessage(ChatColor.GRAY + "West" + ChatColor.DARK_GRAY + "<" + ChatColor.GRAY + String.valueOf(tracker.getDistanceToTrack(Direction.WEST, p.getWorld())) + ChatColor.DARK_GRAY + ">" + ChatColor.GRAY + ": " + westfinal);

                tracker.getNorthPlayers().clear();
                tracker.playersSouth.clear();
                tracker.playersEast.clear();
                tracker.playersWest.clear();

                if(tracker.getType().equals(TrackerType.TEMPORARY)) tracker.clearTempTracker(tracker);
            }else{
                if(!isOnTracker(p)){
                    p.sendMessage(PREFIX + ChatColor.RED + "You are not standing on a valid tracker block.");
                    return true;
                }
                Player victim = Bukkit.getServer().getPlayer(args[0]);

                if(victim == null){
                    p.sendMessage(PREFIX + ChatColor.RED + "Specified player is not online.");
                    return true;
                }

                Tracker tracker = new Tracker(p.getLocation(),p);

                if(!p.getWorld().getName().equals("world")){
                    p.sendMessage(PREFIX + ChatColor.RED + "You must be in the overworld to track!");
                    return true;
                }

                boolean valid = false;

                for(Direction direction : Direction.values()){
                    if(tracker.getDistanceToTrack(direction, p.getWorld()) != 0){
                        valid = true;
                    }
                }

                if(!valid){
                    p.sendMessage(PREFIX + ChatColor.RED + "This tracker is not properly set up and won't track anything.");
                    return true;
                }

                tracker.trackForPlayer(victim);

                StringBuilder northBuilder = new StringBuilder();
                StringBuilder southBuilder = new StringBuilder();
                StringBuilder eastBuilder = new StringBuilder();
                StringBuilder westBuilder = new StringBuilder();

                for(String str : tracker.playersNorth){
                    northBuilder.append(ChatColor.AQUA + str).append(" ");
                }
                for(String str : tracker.playersSouth){
                    southBuilder.append(ChatColor.AQUA + str).append(" ");
                }
                for(String str : tracker.playersEast){
                    eastBuilder.append(ChatColor.AQUA + str).append(" ");
                }
                for(String str : tracker.playersWest){
                    westBuilder.append(ChatColor.AQUA + str).append(" ");
                }

                String northfinal = northBuilder.toString();
                String southfinal = southBuilder.toString();
                String eastfinal = eastBuilder.toString();
                String westfinal = westBuilder.toString();





                p.sendMessage(ChatColor.AQUA + "Tracking results for " + args[0].toLowerCase() + ":");
                if(tracker.getDistanceToTrack(Direction.NORTH, p.getWorld()) != 0) p.sendMessage(ChatColor.GRAY + "North" + ChatColor.DARK_GRAY + "<" + ChatColor.GRAY + String.valueOf(tracker.getDistanceToTrack(Direction.NORTH, p.getWorld())) + ChatColor.DARK_GRAY + ">" + ChatColor.GRAY + ": " + northfinal);
                if(tracker.getDistanceToTrack(Direction.SOUTH, p.getWorld()) != 0) p.sendMessage(ChatColor.GRAY + "South" + ChatColor.DARK_GRAY + "<" + ChatColor.GRAY + String.valueOf(tracker.getDistanceToTrack(Direction.SOUTH, p.getWorld())) + ChatColor.DARK_GRAY + ">" + ChatColor.GRAY + ": " + southfinal);
                if(tracker.getDistanceToTrack(Direction.EAST, p.getWorld()) != 0) p.sendMessage(ChatColor.GRAY + "East" + ChatColor.DARK_GRAY + "<" + ChatColor.GRAY + String.valueOf(tracker.getDistanceToTrack(Direction.EAST, p.getWorld())) + ChatColor.DARK_GRAY + ">" + ChatColor.GRAY + ": " + eastfinal);
                if(tracker.getDistanceToTrack(Direction.WEST, p.getWorld()) != 0) p.sendMessage(ChatColor.GRAY + "West" + ChatColor.DARK_GRAY + "<" + ChatColor.GRAY + String.valueOf(tracker.getDistanceToTrack(Direction.WEST, p.getWorld())) + ChatColor.DARK_GRAY + ">" + ChatColor.GRAY + ": " + westfinal);


                tracker.getNorthPlayers().clear();
                tracker.playersSouth.clear();
                tracker.playersEast.clear();
                tracker.playersWest.clear();

                if(tracker.getType().equals(TrackerType.TEMPORARY)) tracker.clearTempTracker(tracker);

            }




        }
        return true;
    }

    public boolean isOnTracker(Player p){
        return p.getLocation().subtract(0,1,0).getBlock().getType().equals(Material.DIAMOND_BLOCK) || p.getLocation().subtract(0,1,0).getBlock().getType().equals(Material.OBSIDIAN);

    }

}
